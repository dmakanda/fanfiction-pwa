export default {
    install (app, state) {
        Object.defineProperty(app.config.globalProperties, '$state', {
            get: () => state,
        })
    }
}
