import { MoreService, ResponseError } from '@/services/more.service';
// import router from '@/router';

const state = {
    staticPageData: '',
    responseErrorCode: 0,
    responseError: '',
}

const getters = {
    responseErrorCode: (state: { responseErrorCode: string }) => {
        return state.responseErrorCode;
    },
    responseError: (state: { responseError: string }) => {
        return state.responseError;
    }
}

const mutations = {
    dataRequest(state: {
        responseError: string;
        responseErrorCode: number;
    }) {
        state.responseError = "";
        state.responseErrorCode = 0;
    },
    staticPageDataSuccess(state: { staticPageData: string }, payload: any) {
        state.staticPageData = payload;
    },
    dataError(state: {
        responseError: any;
        responseErrorCode: any;
    }, { errorCode, errorMessage }: any) {
        state.responseError = errorMessage;
        state.responseErrorCode = errorCode;
    }
}



const actions = {
    async loadStaticPage(context: any, type: string) {
        context.commit("dataRequest");
        try {
            const resp = await MoreService.staticPages(type);
            context.commit("staticPageDataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async sendContactForm(context: any, data: any) {
        try {
            // await store.dispatch('protectedArea/initCrsf');
            return await MoreService.sendContactMail(data.mail, data.subject, data.message);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async disableAccountUser(context: any) {
        try {
            const result: any = await MoreService.disabledAccount();
            if (result.status === 'ok') {
                // todo
            }
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    }
}

export const more = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
