import { UserService, ResponseError } from '@/services/user.service';

const state = {
    userData: null,
    storiesData: null,
    creatorsData: null,
    fanficsData: null,
    responseErrorCode: 0,
    responseError: '',
}

const getters = {
    currentUser: (state: { userData: any }) => {
        return state.userData;
    },
    responseErrorCode: (state: { responseErrorCode: string }) => {
        return state.responseErrorCode;
    },
    responseError: (state: { responseError: string }) => {
        return state.responseError;
    }
}

const mutations = {
    userDataRequest(state: {
        responseError: string;
        responseErrorCode: number;
    }) {
        state.responseError = "";
        state.responseErrorCode = 0;
    },
    fanficsDataRequest(state: {
        responseError: string;
        responseErrorCode: number;
    }) {
        state.responseError = "";
        state.responseErrorCode = 0;
    },
    userDataSuccess(state: { userData: any }, payload: any) {
        state.userData = payload;
    },
    userDataError(state: {
        responseError: any;
        responseErrorCode: any;
    }, { errorCode, errorMessage }: any) {
        state.responseError = errorMessage;
        state.responseErrorCode = errorCode;
    },
    fanficsDataError(state: {
        responseError: any;
        responseErrorCode: any;
    }, { errorCode, errorMessage }: any) {
        state.responseError = errorMessage;
        state.responseErrorCode = errorCode;
    },
    storiesDataSuccess(state: { storiesData: any }, payload: any) {
        state.storiesData = payload;
    },
    creatorsDataSuccess(state: { creatorsData: any }, payload: any) {
        state.creatorsData = payload;
    },
    fanficsDataSuccess(state: { fanficsData: any }, payload: any) {
        state.fanficsData = payload;
    },
}



const actions = {
    async init ({dispatch}) {
        await dispatch('getUser');
    },
    async getUser(context: any) {
        context.commit("userDataRequest");
        try {
            const resp = await UserService.getUser();
            context.commit("userDataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("userDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async getFollowStories(context: any) {
        context.commit("userDataRequest");
        try {
            const resp = await UserService.getFollowStories();
            context.commit("storiesDataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("userDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async addStory(context: any, data: any) {
        try {
            return await UserService.addStory(data.fanficId, data.fromUser);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async addAuthor(context: any, data: any) {
        try {
            return await UserService.addAuthor(data.userFrom, data.userTo);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async deleteStory(context: any, data: any) {
        try {
            return await UserService.deleteStory(data.fanficId, data.userId);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async deleteAuthor(context: any, data: any) {
        try {
            return await UserService.deleteAuthor(data.userTo, data.userFrom);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async getFollowCreators(context: any) {
        context.commit("userDataRequest");
        try {
            const resp = await UserService.getFollowUsers();
            context.commit("creatorsDataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("userDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async getFavoritesFanfics(context: any, data: any) {
        context.commit("fanficsDataRequest");
        try {
            const resp = await UserService.getFavoritedFanfics(data.username);
            context.commit("fanficsDataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("fanficsDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    }
}

export const user = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
