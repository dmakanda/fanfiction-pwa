import { ProtectAreaService, ResponseError } from '@/services/protected-area.service';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

const state = {
    responseData: '',
    responseErrorCode: 0,
    responseError: '',
}

const getters = {
    responseErrorCode: (state: { responseErrorCode: string }) => {
        return state.responseErrorCode;
    },
    responseError: (state: { responseError: string }) => {
        return state.responseError;
    }
}

const mutations = {
    dataRequest(state: {
        responseError: string;
        responseErrorCode: number;
    }) {
        state.responseError = "";
        state.responseErrorCode = 0;
    },
    dataSuccess(state: { responseData: string }, payload: any) {
        state.responseData = payload;
    },
    dataError(state: {
        responseError: any;
        responseErrorCode: any;
    }, { errorCode, errorMessage }: any) {
        state.responseError = errorMessage;
        state.responseErrorCode = errorCode;
    },
    incrementLike (state) {
        state.responseData.total_likes++
    },
    decrementLike (state) {
        state.responseData.total_likes--
    },
}

const actions = {
    async loadSecretArea(context: any) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.secretArea();
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadFanficsByCategorySlug(context: any, { categorySlug, pageSize, offset }) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getFanficsByCategory(categorySlug, pageSize, offset);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadFanficsByGenre(context: any, { genre, pageSize, offset }) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getFanficsByGenre(genre, pageSize, offset);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadFanficsOrderByKeyword(context: any, { keyword, pageSize, offset }) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getFanficsOrderByKeyword(keyword, pageSize, offset);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadGenres(context: any) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getGenres();
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadCategories(context: any, { pageSize, offset }) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getAllCategories(pageSize, offset);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async searchFanfics(context: any, { fields, pageSize, offset }) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.searchFanfics(fields, pageSize, offset);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadNotifications(context: any, { pageSize, offset}) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getNotifications(pageSize, offset);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async loadFanfic(context: any, { slug, status }) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getFanfic(slug, status);
            context.commit("dataSuccess", resp);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async initCrsf({ dispatch }) {
        await dispatch('loadCSRF');
    },
    async loadCSRF(context: any) {
        context.commit("dataRequest");
        try {
            const resp = await ProtectAreaService.getCSRF();
            context.commit("dataSuccess", resp);
            cookies.set('csrftoken', resp.headers['x-csrftoken']);
            return resp;
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("dataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    }
}

export const protectedArea = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
