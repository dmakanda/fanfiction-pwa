import { store } from '@/store/index';

import { AuthenticationError, AuthService } from '@/services/auth.service';
import { TokenService } from '@/services/token.service';

const state = {
    authenticating: false,
    accessToken: TokenService.getToken(),
    authenticationErrorCode: 0,
    authenticationError: '',
    refreshTokenPromise: null,
}

const getters = {
    authenticationErrorCode: (state: { authenticationErrorCode: any }) => {
        return state.authenticationErrorCode;
    },

    authenticationError: (state: { authenticationError: any }) => {
        return state.authenticationError;
    },

    authenticating: (state: { authenticating: boolean }) => {
        return state.authenticating;
    }
}

const mutations = {
    signinRequest(state: {authenticating: boolean; authenticationError: string; authenticationErrorCode: number }) {
        state.authenticating = true;
        state.authenticationError = "";
        state.authenticationErrorCode = 0;
    },

    signinSuccess(state: { accessToken: string; authenticating: boolean}, accessToken: string) {
        state.accessToken = accessToken;
        state.authenticating = false;
    },

    signinError(state: {authenticating: boolean; authenticationError: string; authenticationErrorCode: number }, { errorCode, errorMessage }: any) {
        state.authenticating = false;
        state.authenticationError = errorCode;
        state.authenticationErrorCode = errorMessage;
    },

    logoutRequest(state: { authenticating: boolean}) {
        state.authenticating = false
    },

    refreshTokenPromise(state: { refreshTokenPromise: any }, promise: any) {
        state.refreshTokenPromise = promise;
    },

    processSuccess(state: { authenticating: boolean}) {
        state.authenticating = false;
    },

    setAutenticatingStatus(state: { authenticating: any}, status: any) {
        state.authenticating = status;
    },
}

const actions = {
    async login(context: any, data: any) {
        context.commit('signinRequest');
        return new Promise((resolve, reject) => {
            AuthService.login(data).then(async res => {
                context.commit('signinSuccess', res);
                resolve(res);
                await store.dispatch('user/init');
            }).catch(err => {
                context.commit('signinError', {
                    errorCode: err.errorCode,
                    errorMessage: err.message
                });
                reject(err.message);
            })
        })
    },

    async logout(context: any) {
        context.commit('logoutRequest');
        return new Promise<void>((resolve) => {
            AuthService.logout();
            resolve();
        });
    },

    refreshToken(context: any, state: { refreshTokenPromise: null }) {
        if (!state.refreshTokenPromise) {
            const p = AuthService.refreshToken();
            context.commit('refreshTokenPromise', p);

            p.then(
                response => {
                    context.commit('refreshTokenPromise', null);
                    context.commit('refreshTokenPromise', response);
                },
                error => {
                    context.commit('refreshTokenPromise', error);
                }
            )
        }
        return state.refreshTokenPromise;
    },

    async signup(context: any, { email, password, username, password2, firstName, lastName}: any) {
        try {
            await AuthService.signup(email, password, username, password2, firstName, lastName);
            context.commit('processSuccess');
            return true;
        } catch (e) {
            if (e instanceof AuthenticationError) {
                context.commit('signinError', {
                    errorCode: e.errorCode,
                    errorMessage: e.message
                });
            }
            return false;
        }
    },

    setAutenticatingStatus(context: any, status: any) {
        context.commit('setAutenticatingStatus', status);
    }

}

export const auth = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
