import { createStore } from 'vuex';
import { auth } from './auth.store';
import { protectedArea } from './protected-area.store';
import { more } from './more.store';
import { user } from './user.store';
import { fanfic } from './fanfic.store';

export const store = createStore({
    strict: true,
    modules: {
        auth,
        protectedArea,
        user,
        more,
        fanfic,
    },
});
