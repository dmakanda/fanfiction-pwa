import { FanficService, ResponseError } from '@/services/fanfic.service';

const state = {
    responseData: null,
    responseErrorCode: 0,
    responseError: '',
}

const getters = {
    responseErrorCode: (state: { responseErrorCode: string }) => {
        return state.responseErrorCode;
    },
    responseError: (state: { responseError: string }) => {
        return state.responseError;
    }
}

const mutations = {
    dataRequest(state: {
        responseError: string;
        responseErrorCode: number;
    }) {
        state.responseError = "";
        state.responseErrorCode = 0;
    },
    responseDataSuccess(state: { responseData: any }, payload: any) {
        state.responseData = payload;
    },
    responseDataError(state: {
        responseError: any;
        responseErrorCode: any;
    }, { errorCode, errorMessage }: any) {
        state.responseError = errorMessage;
        state.responseErrorCode = errorCode;
    }
}



const actions = {
    async favoriteFanfic(context: any, data: any) {
        try {
            return await FanficService.favorite(data.fanficId, data.userId);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("responseDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async shareFanfic(context: any, data: any) {
        try {
            return await FanficService.share(data.fanficId, data.name, data.email, data.to, data.comments);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("responseDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    },
    async unfavoriteFanfic(context: any, data: any) {
        try {
            return await FanficService.unfavorite(data.fanficId, data.userId);
        } catch (e) {
            if (e instanceof ResponseError) {
                context.commit("responseDataError", {
                    errorMessage: e.message,
                    responseErrorCode: e.errorCode
                });
            }
            return e.message;
        }
    }
}

export const fanfic = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
