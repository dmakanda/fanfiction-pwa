import { createI18n } from 'vue-i18n'
import fr from '../lang/fr.json';
import en from '../lang/en.json';
import getBrowserLocale from './get-browser-locale';
import { supportedLocalesInclude } from './supported-locales';

function getStartingLocale() {
  const browserLocale = getBrowserLocale({ countryCodeOnly: true })
  if (supportedLocalesInclude(browserLocale)) {
    return browserLocale
  } else {
    return 'fr'
  }
}

export const i18n = createI18n({
  locale: getStartingLocale(),
  fallbackLocale: 'fr',
  messages: { fr, en }
})
