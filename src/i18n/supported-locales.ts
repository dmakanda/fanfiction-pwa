import supportedLocales from '@/config/supported-locales';

export function getSupportedLocales() {
  const annotatedLocales: any = [];

  for (const code of Object.keys(supportedLocales)) {
    annotatedLocales.push({
      code,
    })
  }

  return annotatedLocales
}

export function supportedLocalesInclude(locale: any) {
  return Object.keys(supportedLocales).includes(locale)
}