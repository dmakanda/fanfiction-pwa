import moment from 'moment'

const filters = {
    dateFormat(value: string) {
        return moment(value).format('DD/MM/YYYY')
    },
    uppercase(value: string) {
        if (typeof value !== "string") {
            throw TypeError("value must be a string")
        }
        return value.toUpperCase();
    },
    lowercase(value: string) {
        if (typeof value !== "string") {
            throw TypeError("value must be a string")
        }
        return value.toLowerCase();
    },

    removeLastComma(str) {
        return str.replace(/,(\s+)?$/, '');
    },
    readMore(text, length, suffix) {
        return text.substring(0, length) + suffix;
    },

    toFixed(price, limit) {
        return price.toFixed(limit);
    },

    toEuro(price) {
        return `$${price}`;
    },

    json(value) {
        return JSON.stringify(value);
    },

    // extract a list of property values
    pluck(objects, key) {
        return objects.map(function (object) {
            return object[key];
        });
    },

    // return the index
    at(value, index) {
        return value[index];
    },

    min(values) {
        return Math.min(...values);
    },

    max(values) {
        return Math.max(...values);
    },

    shuffle(values) {
        for (let i = values.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = values[i];
            values[i] = values[j];
            values[j] = temp;
        }
        return values;
    },

    unique(values) {
        return values.filter(function (element, index, self) {
            return index = self.indexOf(element);
        });
    },

    prepend(string, prepend) {
        return `${string}${prepend}`;
    },

    pluralize(word, amount) {
        return amount > 1 ? `${word}s` : word;
    },

    reverse(value) {
        return value.split('').reverse().join('');
    },

    filterBy(list, value) {
        return list.filter(function (item) {
            return item.indexOf(value) > -1;
        });
    },

    findBy(list, value) {
        return list.filter(function (item) {
            return item == value
        });
    },

    trim(s, len) {
        if (!len) {
            len = 200
        }
        if (s.length < len) {
            return s
        }
        return s.substring(0, len - 3) + '...'
    },

    price(input: number) {
        if (isNaN(input)) {
            return "-";
        }
        return input.toFixed(2) + '€';
    }
}

export default filters;
