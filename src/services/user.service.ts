import ApiService from './api.service';
import { ResponseError } from './helpers';

const UserService = {
    getUser: async function () {
        try {
            return ApiService.get(`/user/`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFollowStories: async function () {
        try {
            return ApiService.get(`/follow-stories/`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    addStory: async function (fanficId: number, fromUser: number) {
        try {
            return ApiService.post('/follow-stories/', {
                'to_fanfic': fanficId,
                'from_user': fromUser,
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    addAuthor: async function (userFrom: number, userTo: number) {
        try {
            return ApiService.post('/follow-user/', {
                'user_from': userFrom,
                'user_to': userTo,
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    deleteStory: async function (fanficId: number, userId: number) {
        try {
            return ApiService.delete(`/story-followed/${fanficId}/`, {
                'from_user': userId
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    deleteAuthor: async function (userTo: number, userFrom: number) {
        try {
            return ApiService.delete(`/author-followed/${userTo}/`, {
                'user_from': userFrom
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFollowUsers: async function () {
        try {
            return ApiService.get(`/follow-user/`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFavoritedFanfics: async function (username: string) {
        try {
            return ApiService.get(`/users/${username}/account/`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    }
}

export { UserService, ResponseError };