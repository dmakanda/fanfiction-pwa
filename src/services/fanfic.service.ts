import ApiService from './api.service';
import { ResponseError } from './helpers';

const FanficService = {
    favorite: async function (fanficId: number, userId: number) {
        try {
            return ApiService.post(`/favorite/`, {
                'id': fanficId,
                'user': userId
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    share: async function (fanficId: number, name: string, email: string, to: string, comments: string) {
        try {
            return ApiService.post(`/share/`, {
                'id': fanficId,
                'name': name,
                'email': email,
                'to': to,
                'comments': comments
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    unfavorite: async function (fanficId: number, userId: number) {
        try {
            return ApiService.post(`/unfavorite/`, {
                'id': fanficId,
                'user': userId
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    }
}

export { FanficService, ResponseError };