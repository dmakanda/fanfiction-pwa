import ApiService from './api.service';
import { TokenService } from './token.service';
import { AxiosRequestConfig } from 'axios';
import qs from 'qs';

class AuthenticationError extends Error {
    errorCode: any;
    constructor(errorCode: any, message: string) {
        super(message);
        this.name = this.constructor.name;
        if (message !== null) {
            this.message = message;
        }
        this.errorCode = errorCode;
    }
}

const AuthService = {
    login: async function (data: any) {
        const requestData: AxiosRequestConfig = {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'Basic : ' + btoa(process.env.VUE_APP_CLIENT_ID + ':' + process.env.VUE_APP_CLIENT_SECRET)
            },
            url: '/oauth2/token/',
            data: qs.stringify({
                'grant_type': 'password',
                username: data.username,
                password: data.password
            })
        };

        try {
            const response = await ApiService.customRequest(requestData);
            TokenService.saveToken(response.data.access_token);
            TokenService.saveRefreshToken(response.data.refresh_token);
            ApiService.setHeader();

            ApiService.mount401Interceptor();

            return response.data.access_token;
        } catch (error) {
            throw new AuthenticationError(
                error.response.status,
                error.response.data.error_description
            );
        }
    },

    refreshToken: async function () {
        const refreshToken = TokenService.getRefreshToken();

        const requestData: AxiosRequestConfig = {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'Basic :' + btoa(process.env.VUE_APP_CLIENT_ID + ':' + process.env.VUE_APP_CLIENT_SECRET)
            },
            url: '/oauth2/token/',
            data: qs.stringify({
                'grant_type': 'refresh_token',
                refreshToken: refreshToken
            })
        };

        try {
            const response = await ApiService.customRequest(requestData);
            TokenService.saveToken(response.data.access_token);
            TokenService.saveRefreshToken(response.data.refresh_token);
            ApiService.setHeader();

            return response.data.access_token;
        } catch (error) {
            throw new AuthenticationError(
                error.response.status,
                error.response.data.error_description
            );
        }
    },

    logout() {
        TokenService.removeToken();
        TokenService.removeRefreshToken();
        ApiService.removeHeader();
        ApiService.unmount401Interceptor();
    },

    signup: async function (email: string, password: string, username: string, password2: string, firstName: string, lastName: string) {
        const data: AxiosRequestConfig = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            url: '/sign_up/',
            data: {
                'email': email,
                'password': password,
                'username': username,
                'password2': password2,
                'first_name': firstName,
                'last_name': lastName
            }
        };

        try {
            return await ApiService.customRequest(data);
        } catch (error) {
            this.catchError(error);
        }
    },

    catchError: function (error: any) {
        let status;
        let description;

        if (error.response === undefined) {
            status = error.message;
            description = error.message;
        } else {
            status = error.response.status;
            description = error.response.data.error.message;
        }

        throw new AuthenticationError(status, description);
    }

}

export { AuthService, AuthenticationError };