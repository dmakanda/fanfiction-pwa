import ApiService from './api.service';
import { ResponseError } from './helpers';

const ProtectAreaService = {
    secretArea: async function () {
        try {
            return ApiService.get('/browse-fanfics/');
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFanficsByCategory: async function (categorySlug: string, pageSize: number, offset: number) {
        try {
            return ApiService.get(`fanfics/?category=${categorySlug}&limit=${pageSize}&offset=${offset}&status=publié`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFanficsByGenre: async function (genre: string, pageSize: number, offset: number) {
        try {
            return ApiService.get(`fanfics/?genres=${genre}&limit=${pageSize}&offset=${offset}&status=publié`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFanficsOrderByKeyword: async function (keyword: string, pageSize: number, offset: number) {
        try {
            return ApiService.get(`fanfics/?ordering=-${keyword}&limit=${pageSize}&offset=${offset}&status=publié`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getGenres: async function () {
        try {
            return ApiService.get('/genres/');
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getFanfic: async function (slug: string, statut?: string) {
        try {
            return ApiService.get(`fanfics/${slug}/detail/?status=${statut}`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getNotifications: async function (pageSize: number, offset: number) {
        try {
            return ApiService.get(`/notifications/?limit=${pageSize}&offset=${offset}`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getCSRF: async function () {
        try {
            return ApiService.get('/csrf/');
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    getAllCategories: async function (pageSize: number, offset: number) {
        try {
            return ApiService.get(`/fanfics/?status=publié&limit=${pageSize}&offset=${offset}`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    searchFanfics: async function (fields: string, pageSize: number, offset: number) {
        try {
            return ApiService.get(`/fanfics/?q=${fields}&status=publié&limit=${pageSize}&offset=${offset}`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    }
}

export { ProtectAreaService, ResponseError };