import ApiService from './api.service';
import { ResponseError } from './helpers';

const MoreService = {
    staticPages: async function (type: string) {
        try {
            return ApiService.get(`/page/${type}/html/`);
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    sendContactMail: async function (userMail: string, subject: string, message: string) {
        try {
            return ApiService.post('/contact-mail/', {
                'from_email': userMail,
                'subject': subject,
                'message': message
            });
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
    disabledAccount: async function () {
        try {
            return ApiService.get('/disable-account/');
        } catch (error) {
            throw new ResponseError(
                error.status,
                error.error.message
            );
        }
    },
}

export { MoreService, ResponseError };