import axios, { AxiosRequestConfig } from 'axios';
import { store } from '@/store';
import { TokenService } from '@/services/token.service';
import { loadingController } from '@ionic/vue';
// import Cookies from 'universal-cookie';
import getCookie from '../cookie';

const csrfToken = getCookie('csrftoken');

const ApiService = {
    _requestInterceptor: 0,
    _401Interceptor: 0,

    init(baseUrl: string | undefined) {
        axios.defaults.baseURL = baseUrl;
    },

    setHeader() {
        axios.defaults.withCredentials = true
        axios.defaults.xsrfCookieName = 'csrfmiddlewaretoken'
        axios.defaults.xsrfHeaderName = 'X-XSRF-TOKEN'

        axios.defaults.headers.common[
            'Authorization'
        ] = `Bearer ${TokenService.getToken()}`

        axios.defaults.headers.common[
            'Content-Type'
        ] = 'application/json'

        if (process.env.NODE_ENV !== 'production') {
            axios.defaults.headers.common[
                'X-CSRFToken'
            ] = csrfToken
        }

    },

    removeHeader() {
        axios.defaults.headers.common = {}
    },

    get(resource: string) {
        return axios.get(resource);
    },

    post(resource: string, data: any) {
        return axios.post(resource, data);
    },

    put(resource: string, data: any) {
        return axios.put(resource, data);
    },

    delete(resource: string, data: any) {
        return axios.delete(resource, { data });
    },

    customRequest(data: AxiosRequestConfig) {
        return axios(data);
    },

    async mountRequestInterceptor() {
        this._requestInterceptor = axios.interceptors.request.use(async config => {
            console.log(config, 'config')
            console.log("show loading");
            const loading = await loadingController.create({
                message: 'Patientez...'
            });
            await loading.present();

            // hack
            loading.dismiss();

            return config;
        });
    },

    mount401Interceptor() {
        this._401Interceptor = axios.interceptors.response.use(
            response => {
                // console.log('response', response);
                loadingController.dismiss().then(r => console.log(r, 'r'));
                return response;
            },
            async error => {
                loadingController.dismiss().then(r => console.log(r, 'r'));
                if (error.request.status === 401) {
                    if (error.config.url.includes('oauth2/token')) {
                        await store.dispatch('auth/logout');
                        throw error;
                    } else {
                        try {
                            await store.dispatch('auth/refresh');
                            return this.customRequest({
                                method: error.config.method,
                                url: error.config.url,
                                data: error.config.data
                            });
                        } catch (e) {
                            throw error;
                        }
                    }
                }
                throw error;
            }
        )
    },

    unmount401Interceptor() {
        axios.interceptors.response.eject(this._401Interceptor);
    }
};

export default ApiService;
