import { TokenService } from '@/services/token.service';
import { createRouter, createWebHashHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Tabs from '../views/Tabs.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/tabs/browse-fanfictions'
  },
  {
    path: '/tabs/',
    component: Tabs,
    children: [
      {
        path: '',
        redirect: '/tabs/browse-fanfictions'
      },
      {
        path: 'browse-fanfictions',
        component: () => import('@/views/Browse.vue')
      },
      {
        path: 'search',
        component: () => import('@/views/Search.vue')
      },
      {
        path: 'dashboard',
        component: () => import('@/views/Dashboard.vue')
      },
      {
        path: 'more',
        component: () => import('@/views/More.vue')
      },
    ]
  },
  {
    path: "/static-page/:slug",
    props: true,
    name: 'static-page',
    component: () => import("@/views/settings/StaticPage.vue"),
  },
  {
    path: "/webview/:url",
    props: true,
    name: 'webview',
    component: () => import("@/views/settings/Webview.vue"),
  },
  {
    path: "/contact",
    component: () => import("@/views/settings/ContactUs.vue"),
  },
  {
    path: "/my-fanfictions",
    component: () => import("@/views/settings/MyFanfictions.vue"),
  },
  {
    path: "/email-change",
    component: () => import("@/views/settings/ChangeEmail.vue"),
  },
  {
    path: "/password-change",
    component: () => import("@/views/settings/ChangePassword.vue"),
  },
  {
    path: "/edit-profile",
    component: () => import("@/views/settings/EditProfile.vue"),
  },
  {
    path: "/sns",
    component: () => import("@/views/settings/SocialNetwork.vue"),
  },
  {
    path: "/username-change",
    component: () => import("@/views/settings/ChangeUsername.vue"),
  },
  {
    path: "/notifications",
    component: () => import("@/views/browse/Notifications.vue"),
  },
  {
    path: "/fanfic/:slug",
    props: true,
    component: () => import("@/views/browse/FanficDetail.vue"),
  },
  {
    path: "/fanfic/:fanficId/chapter/:chapterId",
    props: true,
    component: () => import("@/views/browse/Chapter.vue"),
  },
  {
    path: "/ranking",
    props: route => ({ query: route.query.q }),
    component: () => import("@/views/browse/Ranking.vue"),
  },
  {
    path: '/login',
    component: () => import('@/views/Login.vue'),
    meta: {
      public: true,
      onlyWhenLoggedOut: true
    }
  },
  {
    path: '/signup',
    component: () => import('@/views/Signup.vue'),
    meta: {
      public: true,
      onlyWhenLoggedOut: true
    }
  },
  {
    path: '/home',
    component: () => import('@/views/Welcome.vue'),
    meta: {
      public: true,
      onlyWhenLoggedOut: true
    }
  },
  {
    path: '/:pathMatch(.*)*',
    component: () => import('@/views/NotFound.vue'),
    meta: {
      public: true,
      onlyWhenLoggedOut: false
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(
    record => record.meta.onlyWhenLoggedOut
  );
  const loggedIn = !!TokenService.getToken();

  if (!isPublic && !loggedIn) {
    return next({
      path: "/login",
      query: { redirect: to.fullPath }
    });
  }

  if (loggedIn && onlyWhenLoggedOut) {
    return next("/tabs/browse-fanfictions");
  }

  next();
});

export default router
