# Resources

- <https://www.youtube.com/watch?v=NKL21APReu0>
- <https://github.com/didinj/ionic-5-vue-3-oauth2-login>
- <https://github.com/skiod/ionic-vue/blob/master/src/components/Login.vue>
- <https://stackoverflow.com/questions/53962583/how-to-add-git-hash-to-vue-js-component>
- <https://github.com/PhraseApp-Blog/vue-i18n-demo/tree/master/src>
- <https://www.gethalfmoon.com/docs/page-building/>
- <https://css-tricks.com/considerations-for-making-a-css-framework/>
- <https://v3.vuejs.org/guide/component-dynamic-async.html#dynamic-components-with-keep-alive>
- <https://theroadtoenterprise.com/blog/how-to-build-components-with-design-system-variants-and-share-variant-styles-using-variant-style-provider>
- https://softauthor.com/vue-js-3-composition-api-reusable-scalable-form-validation/
