const { gitDescribe, gitDescribeSync } = require('git-describe');
process.env.VUE_APP_GIT_HASH = gitDescribeSync().hash

module.exports = {
    pwa: {
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: '#38b2ac',
        manifestOptions: {
            name: "Fanfiction",
            short_name: "fanfiction",
            description: "Ecrivez vos propre fanfics (fanfic ou fanfiction), épisodes ou suites de vos animes-séries-films préférés ou inventez, puis mettez-les en ligne et gérez-les vous-même. Partagez-les avec des gens qui aiment les fanfictions.",
            theme_color: "#38b2ac",
            icons: [
                {
                    src: "./img/icons/android-icon-36x36.png",
                    sizes: "36x36",
                    type: "image/png",
                    density: "0.75"
                },
                {
                    src: "./img/icons/android-icon-48x48.png",
                    sizes: "48x48",
                    type: "image/png",
                    density: "1"
                },
                {
                    src: "./img/icons/android-icon-72x72.png",
                    sizes: "72x72",
                    type: "image/png",
                    density: "1.5"
                },
                {
                    src: "./img/icons/android-icon-96x96.png",
                    sizes: "96x96",
                    type: "image/png",
                    density: "2.0"
                },
                {
                    src: "./img/icons/android-icon-192x192.png",
                    sizes: "192x192",
                    type: "image/png",
                    density: "4.0"
                },
                {
                    src: "./img/icons/android-icon-144x144.png",
                    sizes: "144x144",
                    type: "image/png",
                    density: "3.0"
                },
                {
                    src: "./img/icons/android-icon-192x192.png",
                    sizes: "192x192",
                    type: "image/png",
                    purpose: "maskable",
                    density: "4.0"
                },
                {
                    src: "./img/icons/android-icon-144x144.png",
                    sizes: "144x144",
                    type: "image/png",
                    purpose: "maskable",
                    density: "3.0"
                }
            ],
            startUrl: "./index.html",
            display: "standalone",
            background_color: "#4cbab4",
            orientation: "portrait-primary",
        },
        msTileColor: '#38b2ac',
        workboxPluginMode: 'GenerateSW',
        workboxOptions: {
            exclude: [
                /\.map$/,
                /manifest\.json$/
            ],
            navigateFallback: '/index.html',
            runtimeCaching: [
                {
                    urlPattern: new RegExp('/offline'),
                    handler: 'StaleWhileRevalidate',
                },
                {
                    urlPattern: new RegExp('/'),
                    handler: 'StaleWhileRevalidate',
                },
                {
                    urlPattern: new RegExp('^https://fanfiction-fr.herokuapp.com / https://fanfiction-fr.netlify.app / api / '),
                    handler: 'networkFirst',

                    options: {
                        networkTimeoutSeconds: 500,
                        cacheName: 'flight-info-cache',
                        cacheableResponse: {
                            statuses: [0, 200, 404]
                        }
                    }
                }
            ]
        }
    }
}
